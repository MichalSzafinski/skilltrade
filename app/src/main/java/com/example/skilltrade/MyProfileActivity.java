package com.example.skilltrade;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.util.Consumer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.skilltrade.AccountSetupActivity.PICK_IMAGE;

public class MyProfileActivity extends NavDrawerActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Profile myProfile;
    public  static final int EDIT_PROFILE = 4;
    private Bitmap profilePicture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewStub viewStub = findViewById(R.id.layout);
        viewStub.setLayoutResource(R.layout.content_my_profile);
        viewStub.inflate();
        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);


        setTitle(getResources().getString(R.string.title_activity_myprofile));


        DatabaseHelper.getProfile(new Consumer<Profile>() {
            @Override
            public void accept(Profile result) {
                if(result == null)
                {
                    Toast.makeText(getApplicationContext(), "Profile could was not found in database",
                            Toast.LENGTH_LONG).show();
                    finish();
                }
                myProfile = result;
                UpdateText();

                ImageView imageView = findViewById(R.id.profile_image);
                if(result.getGender() == Gender.man)
                    imageView.setImageResource(R.drawable.default_user_male);
                else
                    imageView.setImageResource(R.drawable.default_user_female);

                FloatingActionButton fab = findViewById(R.id.floatingActionButtonEdit);
                fab.show();

                ArrayList<Profile> profileList=new ArrayList<Profile>();
                profileList.add(myProfile);
                DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                    @Override
                    public void accept(List<Bitmap> bitmaps) {
                        if(bitmaps.get(0)!=null)
                        {
                            CircleImageView v = findViewById(R.id.profile_image);
                            v.setImageBitmap(bitmaps.get(0));
                            profilePicture = bitmaps.get(0);
                        }
                    }
                }, profileList);
            }
        });

        FloatingActionButton fab = findViewById(R.id.floatingActionButtonEdit);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
                intent.putExtra("profile", myProfile);
                startActivityForResult(intent, EDIT_PROFILE);
            }
        });
        CircleImageView circleImageView = findViewById(R.id.profile_image);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

                startActivityForResult(intent, PICK_IMAGE);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    Uri profileImageUri = data.getData();
                    try {
                        //InputStream inputStream = getContentResolver().openInputStream(profileImageUri);
                        profilePicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), profileImageUri);
                        CircleImageView profilePictureView = findViewById(R.id.profile_image);
                        if (profilePicture != null)
                        {
                            profilePictureView.setImageBitmap(profilePicture);
                            new DatabaseHelper.UpdateProfileTask(myProfile, new BitmapDrawable(getResources(),profilePicture)).execute();
                        }
                        else
                            new DatabaseHelper.UpdateProfileTask(myProfile, null).execute();
                    } catch (Exception e) {
                        Log.i("MyProfileActivity", "Error while loading image");
                    }
                }
                break;
            case EDIT_PROFILE:
                myProfile = (Profile) data.getSerializableExtra("profile");
                UpdateText();
                if (profilePicture != null)
                {
                    new DatabaseHelper.UpdateProfileTask(myProfile, new BitmapDrawable(getResources(),profilePicture)).execute();
                }
                else
                    new DatabaseHelper.UpdateProfileTask(myProfile, null).execute();
                //new DatabaseHelper.UpdateProfileTask(myProfile, new BitmapDrawable(getResources(),profilePicture)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
        }
    }
    public void UpdateText()
    {
        Profile result = myProfile;

        TextView t;
        t = findViewById(R.id.textViewName);
        String name = result.getName() + " " + result.getSurname();
        t.setText(name);

        t = findViewById(R.id.textViewDescription);
        String descr = getResources().getString(R.string.description) + ": " + result.getDescription();
        if(result.getDescription().length() == 0)
            descr += "-";
        t.setText(descr);

        t = findViewById(R.id.textViewEmail);
        try
        {
            String mail = "Email: " + firebaseUser.getEmail();
            t.setText(mail);
        }
        catch(Exception e) {
            t.setVisibility(View.INVISIBLE);
        }


        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                TextView t = findViewById(R.id.textViewMySkills);
                String mySkills = getResources().getString(R.string.my_skills) + ": ";
                for (String skill: strings) {
                    mySkills += skill + ", ";
                }
                if(strings.size() == 0)
                    mySkills += "-";
                else
                    mySkills = mySkills.substring(0,mySkills.length()-2);
                t.setText(mySkills);
            }
        }, result.getMySkills());

        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                TextView t = findViewById(R.id.textViewDesiredSkills);
                String desiredSkills = getResources().getString(R.string.desired_skills) + ": ";
                for (String skill: strings) {
                    desiredSkills += skill + ", ";
                }
                if(strings.size() == 0)
                    desiredSkills += "-";
                else
                    desiredSkills = desiredSkills.substring(0,desiredSkills.length()-2);
                t.setText(desiredSkills);
            }
        }, result.getDesiredSkills());
    }

   /* @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        switch(id) {
            case R.id.nav_log_out:
                firebaseAuth.signOut();
                break;
            case R.id.nav_main_page:
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.nav_contacts:
                intent = new Intent(this, ChatActivity.class);
                intent.putExtra("myProfile",myProfile);
                intent.putExtra("msg_folder", "1");
                startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    @Override
    public void onBackPressed() {
        if(isTaskRoot())
        {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
        super.onBackPressed();
    }
}
