package com.example.skilltrade;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;

public class EditProfileActivity extends AppCompatActivity {
    public static final int CHOOSE_CURRENT_SKILLS = 1;
    public static final int CHOOSE_DESIRED_SKILLS = 2;

    private Profile myProfile;

    private ArrayList<Integer> currentSkills;
    private ArrayList<Integer> desiredSkills;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                myProfile= null;
            } else {
                myProfile= (Profile) extras.getSerializable("profile");
            }
        } else {
            myProfile= (Profile) savedInstanceState.getSerializable("profile");
        }

        if(myProfile==null)
        {
            Log.e("EditProfileActivity", "myProfile is NULL!!!");
            finish();
        }
        currentSkills = myProfile.getMySkills();
        desiredSkills = myProfile.getDesiredSkills();

        SetText();
        SetClicks();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                //NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void SetText()
    {
        if(myProfile == null)
            return;
        final TextView t = findViewById(R.id.textViewMySkills);
        if(currentSkills!=null)
        {
            DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
                @Override
                public void accept(ArrayList<String> strings) {

                    String mySkills = getResources().getString(R.string.my_skills) + ": ";
                    for (String skill: strings) {
                        mySkills += skill + ", ";
                    }
                    if(currentSkills.size() == 0)
                        mySkills += "-";
                    else
                        mySkills = mySkills.substring(0,mySkills.length()-2);
                    t.setText(mySkills);
                }
            }, currentSkills);
        }
        else
            t.setText(R.string.no_skills);

        final TextView desiredTextView=findViewById(R.id.textViewDesiredSkills);
        if(desiredSkills!=null)
        {
            DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
                @Override
                public void accept(ArrayList<String> strings) {
                    String skillsString = getResources().getString(R.string.desired_skills) + ": ";
                    for (String skill: strings) {
                        skillsString += skill + ", ";
                    }
                    if(desiredSkills.size() == 0)
                        skillsString += "-";
                    else
                        skillsString = skillsString.substring(0,skillsString.length()-2);
                    desiredTextView.setText(skillsString);
                }
            }, desiredSkills);
        }
        else
            desiredTextView.setText(R.string.no_skills);


        TextView d = findViewById(R.id.textViewDescription);
        String descr = myProfile.getDescription();
        if(descr.length()>0)
            d.setText(descr);
    }
    private void SetClicks()
    {
        FloatingActionButton fab = findViewById(R.id.fab_my_skills);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseSkillsActivity.class);
                intent.putExtra("chosenSkills", myProfile.getMySkills());
                startActivityForResult(intent, CHOOSE_CURRENT_SKILLS);
            }
        });

        fab = findViewById(R.id.fab_desired_skills);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseSkillsActivity.class);
                intent.putExtra("chosenSkills", myProfile.getDesiredSkills());
                startActivityForResult(intent, CHOOSE_DESIRED_SKILLS);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHOOSE_CURRENT_SKILLS:
                ArrayList<Integer> currentSkillsList = (ArrayList<Integer>) data.getSerializableExtra("skillsList");
                currentSkills = currentSkillsList;
                SetText();
                break;
            case CHOOSE_DESIRED_SKILLS:
                ArrayList<Integer> desiredSkillsList = (ArrayList<Integer>) data.getSerializableExtra("skillsList");
                desiredSkills = desiredSkillsList;
                SetText();
                break;
        }
    }
    @Override
    public void onBackPressed() {
        TextView t = findViewById(R.id.textViewDescription);
        myProfile.setDesiredSkills(desiredSkills);
        myProfile.setMySkills(currentSkills);
        myProfile.setDescription(t.getText().toString());
        Intent intent = new Intent();
        intent.putExtra("profile", myProfile);
        setResult(MyProfileActivity.EDIT_PROFILE, intent);
        finish();
    }
}
