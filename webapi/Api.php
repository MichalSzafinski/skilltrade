<?php 
require_once dirname(__FILE__).'/includes/DbOperation.php';
 
$response = array();

if(isset($_GET['apicall'])){
 
	switch($_GET['apicall']){
		
		case 'getcurrent':
			if(isset($_GET['id'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['skills'] = $db->getCurrentSkills($_GET['id']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;

		case 'getdesired':
			if(isset($_GET['id'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['skills'] = $db->getDesiredSkills($_GET['id']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;
		
		case 'getskills':
			$db = new DbOperation();
			$response['error'] = false; 
			$response['message'] = 'Request successfully completed';
			$response['skills'] = $db->getSkills();
		break;
		
		case 'getprofile':
			if(isset($_GET['uid'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['profile'] = $db->getProfile($_GET['uid']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;
		
		case 'getnew':
			if(isset($_GET['start']) && isset($_GET['count'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['profiles'] = $db->getNewProfiles($_GET['start'], $_GET['count']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;
		 
		case 'getclosest':
			if(isset($_GET['longitude']) && isset($_GET['latitude']) && isset($_GET['start']) && isset($_GET['count'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['profiles'] = $db->getClosestProfiles($_GET['longitude'], $_GET['latitude'], $_GET['start'], $_GET['count']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;

		case 'getforyou':
			if(isset($_GET['id']) && isset($_GET['longitude']) && isset($_GET['latitude']) && isset($_GET['kmDistance']) && isset($_GET['start']) && isset($_GET['count'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['profiles'] = $db->getForYouProfiles($_GET['id'], $_GET['longitude'], $_GET['latitude'], $_GET['kmDistance'], $_GET['start'], $_GET['count']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;
		
		case 'getcontacts':
			if(isset($_GET['stringid']) && isset($_GET['state'])){
				$db = new DbOperation();
				$response['error'] = false; 
				$response['message'] = 'Request successfully completed';
				$response['profiles'] = $db->getContacts($_GET['stringid'], $_GET['state']);
			}
			else{
				$response['error'] = true;
				$response['message'] = 'Not enough parameters';
			}
		break;
		
		case 'addcontact':
			if(isset($_POST['stringid']) && isset($_POST['contactid'])){
				$db = new DbOperation();
				$result = $db->addContact($_POST['stringid'],$_POST['contactid']);
				if(-1!=$result){
					$response['error'] = false; 
					$response['message'] = 'Request successfully completed';
					$response['result'] = $result;
				}
				else{
					$response['error'] = true; 
					$response['message'] = 'Could not add contact';
				}
			}
		break;
		
		case 'insertskill':
			if(isset($_POST['name'])){
				$db = new DbOperation();
				$result = $db->insertProposedSkill($_POST['name']);
				if($result){
					$response['error'] = false; 
					$response['message'] = 'Request successfully completed';
				}
				else{
					$response['error'] = true; 
					$response['message'] = 'Could not insert skill';
				}
			}
		break;
		
		case 'deletecontact':
			if(isset($_POST['stringid']) && isset($_POST['contactid'])){
				$db = new DbOperation();
				$success = $db->deleteContact($_POST['stringid'],$_POST['contactid']);
				if($success){
					$response['error'] = false; 
					$response['message'] = 'Request successfully completed';
				}
				else{
					$response['error'] = true; 
					$response['message'] = 'Could not delete contact';
				}
			}
		break;
		 
		case 'createprofile':
			if(isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['genderid']) && isset($_POST['stringid'])){
				$db = new DbOperation();
				$success = $db->createProfile($_POST['name'],$_POST['surname'],$_POST['genderid'],$_POST['stringid']);
				if($success){
					$response['error'] = false; 
					$response['message'] = 'Request successfully completed';
				}
				else{
					$response['error'] = true; 
					$response['message'] = 'Could not create profile';
				}
			}
		break;
		
		case 'updateprofile':
			if(isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['genderid']) && isset($_POST['stringid']) &&
				isset($_POST['desc']) && isset($_POST['imgpath']) && isset($_POST['latitude']) && isset($_POST['longitude'])){
				$response['inside'] = true;
				$db = new DbOperation();
				$success = $db->updateProfile($_POST['name'],$_POST['surname'],$_POST['genderid'],$_POST['stringid'],$_POST['desc'],
												$_POST['imgpath'],$_POST['latitude'],$_POST['longitude'],$_POST['cs'],$_POST['ds']);
				if($success){
					$response['error'] = false; 
					$response['message'] = 'Request successfully completed';
				}
				else{
					$response['error'] = true; 
					$response['message'] = 'Could not update profile';
				}
			}
		break;
		
		case 'getgenders':
			$db = new DbOperation();
			$response['error'] = false; 
			$response['message'] = 'Request successfully completed';
			$response['genders'] = $db->getGenders();
		break;
	}

}
else{
	//if it is not api call 
	//pushing appropriate values to response array 
	$response['error'] = true; 
	$response['message'] = 'Invalid API Call';
}
 
echo json_encode($response);

?>