<?php 
	class DbOperation{

		private $pdo; 

		function __construct(){
			require_once dirname(__FILE__).'/DbConnect.php';
			$db = new DbConnect();
			$this->pdo = $db->connect();
		}
		
		private function parseRowToProfile($row){
			$profile = array();
			$profile['Id'] = $row['Id'];
			$profile['Name'] = $row['Name'];
			$profile['Surname'] = $row['Surname'];
			$profile['Description'] = $row['Description'];
			$profile['Gender'] = $row['Gender'];
			$profile['ImagePath'] = $row['ImagePath'];
			$profile['Latitude'] = $row['Latitude'];
			$profile['Longitude'] = $row['Longitude'];
			// TODO
			$profile['Current'] = $this->getCurrentSkills($row['Id']);
			$profile['Desired'] = $this->getDesiredSkills($row['Id']);
			return $profile;
		}
		
		public function getCurrentSkills($id) {
			$skills = array();
			$stmt = $this->pdo->query("SELECT SkillId FROM ProfileSkills WHERE ProfileId={$id} AND WantToLearn=0");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($skills, $row['SkillId']);
			}
			return $skills;
		}
		
		public function getDesiredSkills($id) {
			$skills = array();
			$stmt = $this->pdo->query("SELECT SkillId FROM ProfileSkills WHERE ProfileId={$id} AND WantToLearn=1");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($skills, $row['SkillId']);
			}
			return $skills;
		}
		
		public function getSkills() {
			$skills = array();
			$stmt = $this->pdo->query("SELECT * FROM Skills");
			while ($row = $stmt->fetch()) {
				$skill = array();
				$skill['Id'] = $row['Id'];
				$skill['Name'] = $row['Name'];
				array_push($skills, $skill);
			}
			return $skills;
		}
		
		public function insertProposedSkill($name) {
			$sql = "INSERT INTO ProposedSkills (Name) VALUES ('{$name}')";
			$stmt = $this->pdo->prepare($sql);
			return $stmt->execute();
		}
		
		public function getProfile($uid) {
			$stmt = $this->pdo->query("SELECT Profiles.Id, Profiles.Name, Surname, Description, Genders.Name AS 'Gender', ImagePath, Latitude, Longitude FROM Profiles 
				LEFT JOIN Genders ON Profiles.GenderId=Genders.Id
				WHERE Profiles.StringId='{$uid}'");
			$data = $stmt->fetch();
			return $this->parseRowToProfile($data);
		}
		
		public function getNewProfiles($start, $count) {
			$profiles = array();
			$stmt = $this->pdo->query("SELECT Profiles.Id, Profiles.Name, Surname, Description, Genders.Name AS 'Gender', ImagePath, Latitude, Longitude FROM Profiles 
				LEFT JOIN Genders ON Profiles.GenderId=Genders.Id ORDER BY Profiles.Id DESC
				LIMIT {$start},{$count}");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($profiles, $this->parseRowToProfile($row));
			}
			return $profiles;
		}
		
		public function getClosestProfiles($longitude, $latitude, $start, $count) {
			$profiles = array();
			$stmt = $this->pdo->query("SELECT Profiles.Id, Profiles.Name, Surname, Description, Genders.Name AS 'Gender', ImagePath, Latitude, Longitude FROM Profiles 
				LEFT JOIN Genders ON Profiles.GenderId=Genders.Id 
				ORDER BY (POW(Profiles.Latitude - {$latitude}, 2) + POW((Profiles.Longitude - {$longitude})*COS(RADIANS({$latitude})), 2)) 
				LIMIT {$start},{$count}");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($profiles, $this->parseRowToProfile($row));
			}
			return $profiles;
		}
		
		public function getForYouProfiles($profileId, $longitude, $latitude, $kmDistance, $start, $count) {
			$profiles = array();
			$stmt = $this->pdo->query("SELECT profile.Id, profile.Name, Surname, Description, Genders.Name AS 'Gender', ImagePath, Latitude, Longitude FROM Profiles profile LEFT JOIN Genders ON profile.GenderId=Genders.Id 
				WHERE profile.Id != {$profileId} AND (POW(Latitude - {$latitude}, 2) + POW((Longitude - {$longitude})*COS(RADIANS({$latitude})), 2)) < POW({$kmDistance}/110.25, 2) 
				ORDER BY (CAST(profile.Id IN (SELECT Profiles.Id FROM Profiles LEFT JOIN ProfileSkills hisOwnSkills ON Profiles.Id=hisOwnSkills.ProfileId AND hisOwnSkills.WantToLearn = FALSE 
				JOIN ProfileSkills myLearnSkills ON myLearnSkills.ProfileId = {$profileId} AND myLearnSkills.WantToLearn = TRUE WHERE hisOwnSkills.SkillId = myLearnSkills.SkillId) AS SIGNED INTEGER) + 
				CAST(profile.Id IN (SELECT Profiles.Id FROM Profiles LEFT JOIN ProfileSkills hisLearnSkills ON Profiles.Id=hisLearnSkills.ProfileId AND hisLearnSkills.WantToLearn = TRUE 
				JOIN ProfileSkills myOwnSkills ON myOwnSkills.ProfileId = {$profileId} AND myOwnSkills.WantToLearn = FALSE WHERE hisLearnSkills.SkillId = myOwnSkills.SkillId) 
				AS SIGNED INTEGER)) DESC");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($profiles, $this->parseRowToProfile($row));
			}
			return $profiles;
		}
		
		public function createProfile($name,$surname,$genderId,$stringId) {
			$sql = "INSERT INTO Profiles (Name, Surname, GenderId, StringId) VALUES (?,?,?,?)";
			$stmt = $this->pdo->prepare($sql);
			return $stmt->execute([$name, $surname, $genderId,$stringId]);
		}
		
		public function updateProfile($name,$surname,$genderId,$stringId,$desc,$imgPath,$latitude,$longitude,$cs,$ds) {
			$sql = "UPDATE Profiles SET Name=?, Surname=?, GenderId=?, Description=?, ImagePath=?, Latitude=?, Longitude=? WHERE StringId=?";
			$stmt = $this->pdo->prepare($sql);
			$q1 = $stmt->execute([$name,$surname,$genderId,$desc,$imgPath,$latitude,$longitude,$stringId]);
			// Get profile id
			$stmt = $this->pdo->query("SELECT P.Id FROM Profiles P WHERE P.StringId='{$stringId}'");
			$data = $stmt->fetch();
			$id = $data["Id"];
			// Delete old skills
			$sql = "DELETE FROM ProfileSkills WHERE ProfileId={$id}";
			$q2 = $this->pdo->prepare($sql)->execute();
			// Insert current skills
			if(isset($cs)){
				foreach($cs as $skillid){
				$sql = "INSERT INTO ProfileSkills (ProfileId,SkillId,WantToLearn) VALUES (?,?,0)";
				$stmt= $this->pdo->prepare($sql);
				$stmt->execute([$id,$skillid]);
				}
			}
			// Insert desired skills
			if(isset($ds)){
				foreach($ds as $skillid){
				$sql = "INSERT INTO ProfileSkills (ProfileId,SkillId,WantToLearn) VALUES (?,?,1)";
				$stmt= $this->pdo->prepare($sql);
				$stmt->execute([$id,$skillid]);
				}
			}
			return $q1 && $q2;
		}
		
		// Contacts.State:
		// 0 - outgoing
		// 1 - incoming
		// 2 - accepted by both sides
		
		public function getContacts($uid,$state) {
			$profiles = array();
			$stmt = $this->pdo->query("SELECT Profiles.Id, Profiles.Name, Surname, Description, Genders.Name AS 'Gender', ImagePath, Latitude, Longitude FROM Profiles
				LEFT JOIN Genders ON Profiles.GenderId=Genders.Id WHERE Profiles.Id IN 
				(SELECT C.ContactId FROM Contacts C LEFT JOIN Profiles P ON C.ProfileId=P.Id WHERE P.StringId='{$uid}' AND C.State={$state})");
			$data = $stmt->fetchAll();
			foreach($data as $row){
				array_push($profiles, $this->parseRowToProfile($row));
			}
			return $profiles;
		}
		
		public function addContact($uid,$contactId) {
			// check user invitation state
			$stmt = $this->pdo->query("SELECT State FROM Contacts WHERE ProfileId=(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}') AND ContactId={$contactId}");
			if ($row = $stmt->fetch()) {
				$state = $row['State'];
				// Return if user's already sent invitation or they are friends
				if ($state==0 || $state==2) {
					return false;
				}
				// Accept invitation
				$sql = "UPDATE Contacts SET State=2 WHERE ProfileId=(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}') AND ContactId={$contactId}";
				$q1 = $this->pdo->prepare($sql)->execute();
				$sql = "UPDATE Contacts SET State=2 WHERE ProfileId={$contactId} AND ContactId=(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}')";
				$q2 = $this->pdo->prepare($sql)->execute();
				if ($q1 && $q2) {
					return 2; // users are friends now
				}
				return -1;
			}
			else {
				// Send invitation
				$sql = "INSERT INTO Contacts (ProfileId,ContactId,State) VALUES ((SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}'),{$contactId},0)";
				$q1 = $this->pdo->prepare($sql)->execute();
				$sql = "INSERT INTO Contacts (ProfileId,ContactId,State) VALUES ({$contactId},(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}'),1)";
				$q2 = $this->pdo->prepare($sql)->execute();
				if ($q1 && $q2) {
					return 0; // user sent invitation, but they are not friends yet
				}
				return -1;
			}
		}
		
		public function deleteContact($uid,$contactId) {
			$sql = "DELETE FROM Contacts WHERE ProfileId=(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}') AND ContactId={$contactId}";
			$q1 = $this->pdo->prepare($sql)->execute();
			$sql = "DELETE FROM Contacts WHERE ProfileId={$contactId} AND ContactId=(SELECT P.Id FROM Profiles P WHERE P.StringId='{$uid}')";
			$q2 = $this->pdo->prepare($sql)->execute();
			return $q1 && $q2;
		}
		
		public function getGenders() {
			$genders = array();
			$stmt = $this->pdo->query("SELECT * FROM Genders");
			while ($row = $stmt->fetch()) {
				$gnd = array();
				$gnd['Id'] = $row['Id'];
				$gnd['Name'] = $row['Name'];
				array_push($genders, $gnd);
			}
			return $genders;
		}
	}
?>