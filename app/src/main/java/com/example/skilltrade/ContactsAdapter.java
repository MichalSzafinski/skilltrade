package com.example.skilltrade;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.Consumer;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {
    private Profile[] mDataset;
    private Bitmap[] imageSet;
    private Activity activity;
    private RecyclerView recyclerView;

    public ContactsAdapter(Profile[] myDataset, Activity c, RecyclerView rv)
    {
        activity = c;
        mDataset = myDataset;
        recyclerView = rv;
    }

    @Override
    public ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacts_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                int index = recyclerView.getChildAdapterPosition(v);
//                final Profile p = mDataset[index];
//                final Intent intent = new Intent(activity, ChatActivity.class);
//                DatabaseHelper.getProfile(new Consumer<Profile>() {
//                    @Override
//                    public void accept(Profile profile) {
//                        int firstId = profile.getId();
//                        int secondId = p.getId();
//                        if(secondId<firstId) {
//                            int t = firstId;
//                            firstId = secondId;
//                            secondId = t;
//                        }
//                        intent.putExtra("myProfile",profile);
//                        intent.putExtra("msg_folder", String.valueOf(firstId) + "_" + String.valueOf(secondId));
//                        activity.startActivity(intent);
//                    }
//                });
            }
        });
        ContactsAdapter.MyViewHolder vh = new ContactsAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ContactsAdapter.MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Profile profile = mDataset[position];
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("profile", profile);
            }
        });
        holder.textViewName.setText(profile.getName());
        holder.textViewSurname.setText(profile.getSurname());
        if(imageSet!=null && position<imageSet.length && imageSet[position] != null)
        {
            holder.imageView.setImageBitmap(imageSet[position]);
        }
        else if(profile.getGender() == Gender.man)
            holder.imageView.setImageResource(R.drawable.default_user_male);
        else
            holder.imageView.setImageResource(R.drawable.default_user_female);
//        holder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(activity, ProfileActivity.class);
//                intent.putExtra("profile", profile);
//            }
//        });
        holder.imageChat.setVisibility(View.VISIBLE);
        holder.imageChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = recyclerView.getChildAdapterPosition(holder.rootView);
                final Profile p = mDataset[index];
                final Intent intent = new Intent(activity, ChatActivity.class);
                DatabaseHelper.getProfile(new Consumer<Profile>() {
                    @Override
                    public void accept(Profile profile) {
                        int firstId = profile.getId();
                        int secondId = p.getId();
                        if(secondId<firstId) {
                            int t = firstId;
                            firstId = secondId;
                            secondId = t;
                        }
                        intent.putExtra("myProfile",profile);
                        intent.putExtra("msg_folder", String.valueOf(firstId) + "_" + String.valueOf(secondId));
                        activity.startActivity(intent);
                    }
                });
            }
        });
        holder.imageDelete.setVisibility(View.VISIBLE);
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = recyclerView.getChildAdapterPosition(holder.rootView);
                final Profile p = mDataset[index];
                final int contactId = p.getId();
                new AlertDialog.Builder(activity)
                        .setTitle("Delete contact")
                        .setMessage("Are you sure you want to delete this contact?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                new DatabaseHelper.DeleteContactTask().execute(contactId);
                                Profile[] newDataset = new Profile[mDataset.length-1];
                                int i = 0;
                                for(Profile sp : mDataset) {
                                    if(sp.getId()!=p.getId()) {
                                        newDataset[i++] = sp;
                                    }
                                }
                                mDataset = newDataset;
                                ContactsAdapter.this.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no,null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public void setDataset(Profile[] mDataset) {
        this.mDataset = mDataset;
    }

    public void setImageSet(Bitmap[] imageSet) {
        this.imageSet = imageSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView textViewName;
        public TextView textViewSurname;
        public ImageView imageView;
        public ImageView imageDelete;
        public ImageView imageAdd;
        public ImageView imageChat;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            textViewName = v.findViewById(R.id.textViewName);
            textViewSurname = v.findViewById(R.id.textViewSurname);
            imageView = v.findViewById(R.id.imageView);
            imageDelete = v.findViewById(R.id.imageDelete);
            imageAdd = v.findViewById(R.id.imageAdd);
            imageChat = v.findViewById(R.id.imageChat);
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
}
