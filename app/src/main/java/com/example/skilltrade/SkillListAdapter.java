package com.example.skilltrade;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.skilltrade.utilities.CustomCheckBox;

import java.util.ArrayList;

public class SkillListAdapter extends RecyclerView.Adapter<SkillListAdapter.MyViewHolder> {

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomCheckBox mCheckBox;

        public MyViewHolder(View v) {
            super(v);
            mCheckBox = v.findViewById(R.id.checkBox);
        }
    }

    private SkillListItem[] mDataset;

    public SkillListAdapter(SkillListItem[] dataset) {
        mDataset = dataset;
    }

    @Override
    public SkillListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_list_item, parent, false);

        SkillListAdapter.MyViewHolder vh = new SkillListAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SkillListAdapter.MyViewHolder holder, final int position) {
        final CustomCheckBox checkBox = holder.mCheckBox;
        checkBox.setText(mDataset[position].getName());
        checkBox.setChecked(mDataset[position].isChecked());
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDataset[position].setChecked(checkBox.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public ArrayList<Integer> getCheckedSkills() {
        ArrayList<Integer> list = new ArrayList<>();
        for (SkillListItem skill : mDataset) {
            if (skill.isChecked()) {
                list.add(skill.getId());
            }
        }
        return list;
    }
}
