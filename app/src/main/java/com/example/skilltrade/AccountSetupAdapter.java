package com.example.skilltrade;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Consumer;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class AccountSetupAdapter extends PagerAdapter {
    AccountSetupActivity mContext;
    ImageView profilePictureView;
    TextView description;
    TextView currentSkills;
    TextView desiredSkills;
    private int itemsCount = 4;

    public AccountSetupAdapter(AccountSetupActivity context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return itemsCount;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        switch (position) {
            case 0:
                return createSetProfilePicturePage(container);
            case 1:
                return createWriteDescriptionPage(container);
            case 2:
                return createChooseCurrentSkillsPage(container);
            case 3:
                return createChooseDesiredSkillsPage(container);
        }
        return new ScrollView(mContext);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ScrollView) object);
    }

    private View createSetProfilePicturePage(ViewGroup container) {
        View pageView = LayoutInflater.from(mContext).inflate(R.layout.set_profile_picture_page, container, false);
        profilePictureView = pageView.findViewById(R.id.profile_image);
        if (mContext.getProfilePicture() != null) {
            profilePictureView.setImageDrawable(mContext.getProfilePicture());
        } else {
            if (mContext.getUserProfile().getGender() != null) {
                Gender gender = mContext.getUserProfile().getGender();
                switch (gender) {
                    case man:
                        profilePictureView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.default_user_male));
                        break;
                    case woman:
                        profilePictureView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.default_user_female));
                        break;
                }
            }
        }
        Button btnLoadPhoto = pageView.findViewById(R.id.btn_load_image);
        btnLoadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

                mContext.startActivityForResult(intent, mContext.PICK_IMAGE);
            }
        });
        container.addView(pageView);
        return pageView;
    }

    private View createWriteDescriptionPage(ViewGroup container) {
        View pageView = LayoutInflater.from(mContext).inflate(R.layout.write_description_page, container, false);
        description = pageView.findViewById(R.id.input_description);
        description.setText(mContext.getDescription());
        container.addView(pageView);
        return pageView;
    }

    private View createChooseCurrentSkillsPage(ViewGroup container) {
        View pageView = LayoutInflater.from(mContext).inflate(R.layout.choose_current_skills_page, container, false);
        currentSkills = pageView.findViewById(R.id.current_skills_view);
        if (mContext.getCurrentSkills() != null) {
            setCurrentSkills(mContext.getCurrentSkills());
        }
        FloatingActionButton fab = pageView.findViewById(R.id.fab_add_skills);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext.getApplicationContext(), ChooseSkillsActivity.class);
                intent.putExtra("chosenSkills", mContext.getCurrentSkills());
                mContext.startActivityForResult(intent, mContext.CHOOSE_CURRENT_SKILLS);
            }
        });
        container.addView(pageView);
        return pageView;
    }

    private View createChooseDesiredSkillsPage(ViewGroup container) {
        View pageView = LayoutInflater.from(mContext).inflate(R.layout.choose_desired_skills_page, container, false);
        desiredSkills = pageView.findViewById(R.id.desired_skills_view);
        if (mContext.getDesiredSkills() != null) {
            setDesiredSkills(mContext.getDesiredSkills());
        }
        FloatingActionButton fab = pageView.findViewById(R.id.fab_add_skills);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext.getApplicationContext(), ChooseSkillsActivity.class);
                intent.putExtra("chosenSkills", mContext.getDesiredSkills());
                mContext.startActivityForResult(intent, mContext.CHOOSE_DESIRED_SKILLS);
            }
        });
        container.addView(pageView);
        return pageView;
    }

    public void setProfilePicture(Drawable drawable) {
        if (profilePictureView != null)
            profilePictureView.setImageDrawable(drawable);
    }

    public String getDescription() {
        if (description == null)
            return "";
        else
            return description.getText().toString();
    }

    public void setCurrentSkills(ArrayList<Integer> skills) {
        if (currentSkills != null) {
            if (skills == null || skills.isEmpty()) {
                currentSkills.setText(R.string.no_skills);
                return;
            }
            DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
                @Override
                public void accept(ArrayList<String> strings) {
                    StringBuilder sb = new StringBuilder();
                    for (String skill : strings) {
                        sb.append(skill);
                        sb.append(", ");
                    }
                    sb.setLength(sb.length() - 2);
                    currentSkills.setText(sb.toString());
                }
            }, skills);
        }
    }

    public void setDesiredSkills(ArrayList<Integer> skills) {
        if (desiredSkills != null) {
            if (skills == null || skills.isEmpty()) {
                desiredSkills.setText(R.string.no_skills);
                return;
            }
            DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
                @Override
                public void accept(ArrayList<String> strings) {
                    StringBuilder sb = new StringBuilder();
                    for (String skill : strings) {
                        sb.append(skill);
                        sb.append(", ");
                    }
                    sb.setLength(sb.length() - 2);
                    desiredSkills.setText(sb.toString());
                }
            }, skills);
        }
    }
}
