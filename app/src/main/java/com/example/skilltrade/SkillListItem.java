package com.example.skilltrade;

public class SkillListItem {
    private Integer id;
    private String name;
    private boolean isChecked;

    public SkillListItem(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.isChecked = false;
    }

    public SkillListItem(Integer id, String name, boolean isChecked) {
        this.id = id;
        this.name = name;
        this.isChecked = isChecked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
