package com.example.skilltrade;

import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v4.util.Consumer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.skilltrade.utilities.DatabaseHelper;

import java.util.ArrayList;

public class ChooseSkillsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private SkillListAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar mProgressBar;
    private ArrayList<Integer> chosenSkillsIds;

    private Consumer<ArrayList<String>> fillRecyclerView = new Consumer<ArrayList<String>>() {
        @Override
        public void accept(ArrayList<String> allSkills) {
            ArrayList<SkillListItem> chosenSkills = new ArrayList<>();
            int i = 1; // TODO moga byc niespojne dane
            for (String skillName : allSkills) {
                chosenSkills.add(new SkillListItem(i, skillName));
                i++;
            }
            if(chosenSkillsIds!=null) {
                for (Integer skillId : chosenSkillsIds) {
                    for (SkillListItem skill : chosenSkills) {
                        if (skill.getId()==skillId) {
                            skill.setChecked(true);
                            break;
                        }
                    }
                }
            }
            SkillListItem[] skillsArray = new SkillListItem[chosenSkills.size()];
            mAdapter = new SkillListAdapter(chosenSkills.toArray(skillsArray));
            recyclerView.setAdapter(mAdapter);
            mProgressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_skills);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        if(getIntent().getExtras().containsKey("chosenSkills"))
            chosenSkillsIds = (ArrayList<Integer>) getIntent().getSerializableExtra("chosenSkills");

        mProgressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DatabaseHelper.getSkills(fillRecyclerView,null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose_skills_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra("skillsList", mAdapter.getCheckedSkills());
                setResult(AccountSetupActivity.CHOOSE_CURRENT_SKILLS, intent);
                finish();
                return true;
            case R.id.action_add_skill:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.add_skill_title);

                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE);

                builder.setView(input);
                builder.setPositiveButton(R.string.add_skill_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = input.getText().toString();
                        new DatabaseHelper.InsertSkillTask().execute(name);
                        Toast.makeText(getApplicationContext(),"Skill proposed succesfully",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton(R.string.add_skill_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("skillsList", mAdapter.getCheckedSkills());
        setResult(AccountSetupActivity.CHOOSE_CURRENT_SKILLS, intent);
        finish();
    }
}
