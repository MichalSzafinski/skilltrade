package com.example.skilltrade.utilities;

public class ApiCall {

    private static final String ROOT_URL = "https://skilltrade-bacdd.appspot.com/?apicall=";
    public static final String GET_CURRENT_SKILLS_URL = ROOT_URL + "getcurrent&";
    public static final String GET_DESIRED_SKILLS_URL = ROOT_URL + "getdesired&";
    public static final String GET_SKILLS_URL = ROOT_URL + "getskills";
    public static final String GET_GENDERS_URL = ROOT_URL + "getgenders";
    public static final String GET_PROFILE_URL = ROOT_URL + "getprofile&";
    public static final String GET_NEW_PROFILES_URL = ROOT_URL + "getnew&";
    public static final String GET_CLOSEST_PROFILES_URL = ROOT_URL + "getclosest&";
    public static final String GET_FOR_YOU_PROFILES_URL = ROOT_URL + "getforyou&";
    public static final String CREATE_PROFILE_URL = ROOT_URL + "createprofile";
    public static final String UPDATE_PROFILE_URL = ROOT_URL + "updateprofile";
    public static final String GET_CONTACTS_URL = ROOT_URL + "getcontacts&";
    public static final String ADD_CONTACT_URL = ROOT_URL + "addcontact";
    public static final String DELETE_CONTACT_URL = ROOT_URL + "deletecontact";
    public static final String INSERT_SKILL_URL = ROOT_URL + "insertskill";

}
