package com.example.skilltrade;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Consumer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InvitationsTabFragment extends Fragment {
    private View myView;
    private ProgressBar mProgressBar;
    private RecyclerView recyclerViewIncoming;
    private RecyclerView recyclerViewOutgoing;
    private InvitationsAdapter mIncomingAdapter;
    private InvitationsAdapter mOutgoingAdapter;
    private LinearLayout layoutIncoming;
    private LinearLayout layoutOutgoing;
    public Profile[] incomingInvitations = new Profile[]{};
    public Profile[] outgoingInvitations = new Profile[]{};
    public Bitmap[] incomingImageset;
    public Bitmap[] outgoingImageset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_invitations, container, false);
        myView = rootView;

        mProgressBar = rootView.findViewById(R.id.progressBar);
        recyclerViewIncoming = rootView.findViewById(R.id.recyclerViewIncoming);
        recyclerViewOutgoing = rootView.findViewById(R.id.recyclerViewOutgoing);
        layoutIncoming = rootView.findViewById(R.id.linearLayoutIncoming);
        layoutOutgoing = rootView.findViewById(R.id.linearLayoutOutgoing);
        mIncomingAdapter = new InvitationsAdapter(incomingInvitations,getActivity(),recyclerViewIncoming,InvitationsAdapter.Type.Incoming);
        mOutgoingAdapter = new InvitationsAdapter(outgoingInvitations,getActivity(),recyclerViewOutgoing,InvitationsAdapter.Type.Outgoing);
        recyclerViewIncoming.setAdapter(mIncomingAdapter);
        recyclerViewOutgoing.setAdapter(mOutgoingAdapter);
        recyclerViewIncoming.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewOutgoing.setLayoutManager(new LinearLayoutManager(getContext()));

        final SwipeRefreshLayout swipeRefreshLayout = rootView.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DatabaseHelper.getContacts(new Consumer<ArrayList<Profile>>() {
                    @Override
                    public void accept(ArrayList<Profile> profilesList) {
                        UpdateIncomingInvitations(profilesList);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                },1,true);
            }
        });

        DatabaseHelper.getContacts(new Consumer<ArrayList<Profile>>() {
            @Override
            public void accept(ArrayList<Profile> profilesList) {
                UpdateOutgoingInvitations(profilesList);
            }
        },0,false);
        DatabaseHelper.getContacts(new Consumer<ArrayList<Profile>>() {
            @Override
            public void accept(ArrayList<Profile> profilesList) {
                UpdateIncomingInvitations(profilesList);
            }
        },1,true);

        return rootView;
    }

    private void UpdateIncomingInvitations(ArrayList<Profile> profilesList) {
        Profile[] profiles = profilesList.toArray(new Profile[profilesList.size()]);
        incomingInvitations = profiles;
        if(mIncomingAdapter != null) {
            mIncomingAdapter.setDataset(profiles);
            mIncomingAdapter.notifyDataSetChanged();
            if(myView != null)
            {
                mProgressBar.setVisibility(View.GONE);
                layoutIncoming.setVisibility(View.VISIBLE);
            }
            if(profiles.length==0)
                return;
            DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                @Override
                public void accept(List<Bitmap> bitmaps) {
                    incomingImageset = new Bitmap[bitmaps.size()];
                    for (int i=0;i<bitmaps.size();i++)
                    {
                        incomingImageset[i] = bitmaps.get(i);
                    }
                    mIncomingAdapter.setImageSet(incomingImageset);
                    mIncomingAdapter.notifyDataSetChanged();
                }
            }, Arrays.asList(profiles));
        }
    }

    private void UpdateOutgoingInvitations(ArrayList<Profile> profilesList) {
        Profile[] profiles = profilesList.toArray(new Profile[profilesList.size()]);
        outgoingInvitations = profiles;
        if(mOutgoingAdapter != null) {
            mOutgoingAdapter.setDataset(profiles);
            mOutgoingAdapter.notifyDataSetChanged();
            if(myView != null)
            {
                mProgressBar.setVisibility(View.GONE);
                layoutOutgoing.setVisibility(View.VISIBLE);
            }
            if(profiles.length==0)
                return;
            DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                @Override
                public void accept(List<Bitmap> bitmaps) {
                    outgoingImageset = new Bitmap[bitmaps.size()];
                    for (int i=0;i<bitmaps.size();i++)
                    {
                        outgoingImageset[i] = bitmaps.get(i);
                    }
                    mOutgoingAdapter.setImageSet(outgoingImageset);
                    mOutgoingAdapter.notifyDataSetChanged();
                }
            }, Arrays.asList(profiles));
        }
    }
}
