package com.example.skilltrade;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Consumer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContactsTabFragment extends Fragment {
    private View myView;
    private ProgressBar mProgressBar;
    private RecyclerView recyclerView;
    private ContactsAdapter mAdapter;
    public Profile[] dataset = new Profile[]{};
    public Bitmap[] imageset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_contacts, container, false);
        myView = rootView;

        mProgressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        mAdapter = new ContactsAdapter(dataset, getActivity(), recyclerView);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        DatabaseHelper.getContacts(new Consumer<ArrayList<Profile>>() {
            @Override
            public void accept(ArrayList<Profile> profiles) {
                UpdateData(profiles.toArray(new Profile[profiles.size()]));
            }
        },2, false);

        final SwipeRefreshLayout swipeRefreshLayout = rootView.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DatabaseHelper.getContacts(new Consumer<ArrayList<Profile>>() {
                    @Override
                    public void accept(ArrayList<Profile> profiles) {
                        UpdateData(profiles.toArray(new Profile[profiles.size()]));
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(),"WORKS",Toast.LENGTH_LONG);
                    }
                },2, true);
            }
        });

        return rootView;
    }

    public Boolean UpdateData(Profile[] profiles)
    {
        dataset = profiles;
        if(mAdapter != null) {
            mAdapter.setDataset(profiles);
            mAdapter.notifyDataSetChanged();
            if(myView != null)
            {
                mProgressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
            if(profiles.length==0)
                return false;
            DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                @Override
                public void accept(List<Bitmap> bitmaps) {
                    imageset = new Bitmap[bitmaps.size()];
                    for (int i=0;i<bitmaps.size();i++)
                    {
                        imageset[i] = bitmaps.get(i);
                    }
                    mAdapter.setImageSet(imageset);
                    mAdapter.notifyDataSetChanged();
                }
            }, Arrays.asList(profiles));
            return true;
        }
        return false;
    }
}
