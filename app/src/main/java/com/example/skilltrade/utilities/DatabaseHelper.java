package com.example.skilltrade.utilities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Consumer;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.skilltrade.NavDrawerActivity;
import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

import javax.net.ssl.HttpsURLConnection;

public class DatabaseHelper {

    private static final int GET_REQUEST = 1024;
    private static final int POST_REQUEST = 1025;

    private static HashMap<Integer,String> skills = null;
    private static ArrayList<String> skillNames = null;
    private static ArrayList<Profile> newProfiles = null;
    private static ArrayList<Profile> closestProfiles = null;
    private static ArrayList<Profile> forYouProfiles = null;
    private static ArrayList<Profile> contacts = null;
    private static ArrayList<Profile> contactsIncoming = null;
    private static ArrayList<Profile> contactsOutgoing = null;
    private static Profile sProfile = null;
    private static Location userLocation = null;

    private static double lastLongitude = 0;
    private static double lastLatitude = 0;
    private static int lastProfileId = 0;
    private static int searchRadius = 60;

    private static HashMap<String, Bitmap> images = new HashMap<>();

    public static boolean cleared = false;

    public static void ClearData() {
        skills = null;
        skillNames = null;
        newProfiles = null;
        closestProfiles = null;
        forYouProfiles = null;
        contacts = null;
        contactsIncoming = null;
        contactsOutgoing = null;
        sProfile = null;
        userLocation = null;
        lastLongitude = 0;
        lastLatitude = 0;
        lastProfileId = 0;
        cleared = true;
    }

    public static int getSearchRadius() {
        return searchRadius;
    }

    public static void setSearchRadius(int searchRadius) {
        DatabaseHelper.searchRadius = searchRadius;
    }

    /**
     * Gets skills with specified ids from database
     * Gets all skills if ids is null
     */
    public static void getSkills(final Consumer<ArrayList<String>> function, ArrayList<Integer> ids) {
        if (skills == null) {
            new GetSkillsTask(function, ids).execute();
        }
        else if(function != null) {
            if(ids == null) {
                function.accept(skillNames);
            }
            else {
                ArrayList<String> chosenSkills = new ArrayList<>();
                for(Integer id : ids) {
                    chosenSkills.add(skills.get(id));
                }
                function.accept(chosenSkills);
            }
        }
    }
    public static Location getUserLocation() {
        return userLocation;
    }

    public static void setUserLocation(Location userLocation) {
        DatabaseHelper.userLocation = userLocation;
    }
    /**
     * Gets current user profile
     */
    public static void getProfile(final Consumer<Profile> function) {
        if(sProfile!=null) {
            function.accept(sProfile);
        }
        else {
            FirebaseAuth auth = FirebaseAuth.getInstance();
            if(auth!=null) {
                FirebaseUser user = auth.getCurrentUser();
                if(user!=null) {
                    String uid = user.getUid();
                    new GetProfileTask(function).execute(uid);
                }
            }
        }
    }

    /**
     * Gets first 'count' newest profiles from database
     */
    public static void getNewProfiles(final Consumer<ArrayList<Profile>> function, int myId, int count) {
        if (newProfiles == null) {
            new GetNewProfilesTask(function,myId,0,count).execute();
        }
        else if(count > newProfiles.size()) {
            int startIndex = newProfiles.size();
            count -= startIndex;
            new GetNewProfilesTask(function,myId,startIndex,count).execute();
        }
        else if(function != null) {
            function.accept(newProfiles);
        }
    }
    public static void getImages(final Consumer<List<Bitmap>> function, List<Profile> profiles)
    {
        if(profiles==null || profiles.size()==0)
            return;
        ArrayList<Profile> imagesToDownload = new ArrayList<>();

        for (Profile profile: profiles)
        {
            if(!images.containsKey(profile.getImagePath()) && profile.getImagePath().length()>0)
            {
                imagesToDownload.add(profile);
            }
        }

        if(imagesToDownload.size()>0)
        {
//            new DownloadImagesTask(function, profiles).execute(imagesToDownload);
            new DownloadImagesTask(function, profiles).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,imagesToDownload);
        }
        else
        {
            ArrayList<Bitmap> imagesOut = new ArrayList<>();
            for (Profile profile: profiles)
            {
                if(images.containsKey(profile.getImagePath())) {
                    imagesOut.add(images.get(profile.getImagePath()));
                }
                else
                    imagesOut.add(null);
            }
            if(function!=null)
                function.accept(imagesOut);
        }

    }
    /**
     * Downloads images for profiles in the list and adds them to images hashmap,
     * profiles - those that need downloading image
     * profilesIn - profiles that were passed to getImages
     */
    private static class DownloadImagesTask extends AsyncTask<ArrayList<Profile>, Void, ArrayList<Bitmap>> {
        Consumer<List<Bitmap>> function;
        List<Profile> profilesIn;

        DownloadImagesTask(Consumer<List<Bitmap>> function, List<Profile> profilesIn) {
            this.function = function;
            this.profilesIn = profilesIn;
        }
        @Override
        protected ArrayList<Bitmap> doInBackground(ArrayList<Profile>... params) {
            Log.i("ScreenSlideFragment","Downloading images................");
            ArrayList<Profile> profiles = params[0];
            final Bitmap[] tab = new Bitmap[profiles.size()];
            int i=0;
            for (Profile p: profiles)
            {
                if(p.getImagePath()==null || p.getImagePath().length()<4)
                {
                    i++;
                    continue;
                }
                try {
                    final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child(p.getImagePath());

                    final int ind = i;

                    final CountDownLatch done = new CountDownLatch(1);
                    final File localFile = File.createTempFile("images", "jpg");
                    islandRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            tab[ind] = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                            localFile.delete();
                            done.countDown();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.e("MY ERROR_____________", exception.getMessage());
                            done.countDown();
                        }
                    });
                    try {
                        done.await();
                    } catch (Exception e) {
                        Log.e("MY ERROR_____________", e.getMessage());
                    }
                } catch (IOException e) {
                    Log.e("Image upload error", e.getMessage());
                }
                if(cleared == false && tab[i]!=null && images!=null && !images.containsKey(p.getImagePath()))
                {
                    images.put(p.getImagePath(), tab[i]);
                }
                i++;
            }
            if(cleared)
                return null;
            ArrayList<Bitmap> imagesOut = new ArrayList<>();
            for (Profile profile: profilesIn)
            {
                if(images.containsKey(profile.getImagePath())) {
                    imagesOut.add(images.get(profile.getImagePath()));
                }
                else
                    imagesOut.add(null);
            }

            return imagesOut;
        }

        @Override
        protected void onPostExecute(ArrayList<Bitmap> imagesOut) {
            if(function!=null && cleared == false)
                function.accept(imagesOut);
            super.onPostExecute(imagesOut);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    /**
     * Gets first 'count' profiles closest to given 'longitude' and 'latitude' from database
     */
    public static void getClosestProfiles(final Consumer<ArrayList<Profile>> function, int myId, double longitude, double latitude, int count) {

        if (closestProfiles == null || lastLongitude != longitude || lastLatitude != latitude) {
            new GetClosestProfilesTask(function,myId,longitude,latitude,0,count).execute();
        }
        else if(count > closestProfiles.size()) {
            int startIndex = closestProfiles.size();
            count -= startIndex;
            new GetClosestProfilesTask(function,myId,longitude,latitude,startIndex,count).execute();
        }
        else if(function != null) {
            function.accept(closestProfiles);

        }
        lastLongitude = longitude;
        lastLatitude = latitude;
    }

    /**
     * Gets first 'count' best profiles for user with given 'profileId' from database
     */
    public static void getForYouProfiles(final Consumer<ArrayList<Profile>> function, int profileId, double longitude, double latitude, int count) {
        getForYouProfiles(function,profileId,longitude,latitude,count,false);
    }

    public static void getForYouProfiles(final Consumer<ArrayList<Profile>> function, int profileId, double longitude, double latitude, int count, boolean forceUpdate) {
        if (forceUpdate || forYouProfiles == null || lastProfileId != profileId) {
            new GetForYouProfilesTask(function,profileId,longitude,latitude,0,count).execute();
        }
        else if(count > forYouProfiles.size()) {
            int startIndex = forYouProfiles.size();
            count -= startIndex;
            new GetForYouProfilesTask(function,profileId,longitude,latitude,startIndex,count).execute();
        }
        else if(function != null) {
            function.accept(forYouProfiles);
        }
        lastProfileId = profileId;
    }

    // 0 - outgoing invitations
    // 1 - incoming invitations
    // 2 - friends
    public static void getContacts(final Consumer<ArrayList<Profile>> function, int option, boolean forceUpdate) {
        ArrayList<Profile> profiles = null;
        switch(option) {
            case 0:
                profiles = contactsOutgoing;
                break;
            case 1:
                profiles = contactsIncoming;
                break;
            case 2:
                profiles = contacts;
                break;
        }
        if (profiles == null || forceUpdate) {
            new GetContactsTask(function,option).execute();
        }
        else if(function != null) {
            function.accept(profiles);
        }
    }

    public static class InsertSkillTask extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... strings) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("name", String.valueOf(strings[0]));
            String response = requestHandler.sendPostRequest(ApiCall.INSERT_SKILL_URL, params);
            return response;
        }
    }

    private static class GetContactsTask extends AsyncTask<Void, Void, String> {

        Consumer<ArrayList<Profile>> function;
        int option;

        GetContactsTask(Consumer<ArrayList<Profile>> function, int option) {
            this.function = function;
            this.option = option;
        }
        @Override
        protected String doInBackground(Void... voids) {
            String uid = FirebaseAuth.getInstance().getUid();
            if(uid == null) {
                return null;
            }
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("stringid", uid);
            params.put("state", String.valueOf(option));
            String response = requestHandler.sendGetRequest(ApiCall.GET_CONTACTS_URL, params);
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    JSONArray jsonArray = object.getJSONArray("profiles");
                    ArrayList<Profile> profiles = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        profiles.add(JSONtoProfile(obj));
                    }
                    switch(option) {
                        case 0:
                            contactsOutgoing = profiles;
                            break;
                        case 1:
                            contactsIncoming = profiles;
                            break;
                        case 2:
                            contacts = profiles;
                            break;
                    }
                    if(function!=null)
                        function.accept(profiles);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class AddContactTask extends AsyncTask<Profile,Void,Boolean> {
        @Override
        protected Boolean doInBackground(Profile... profiles) {
            Profile contactProfile = profiles[0];
            Integer contactId = contactProfile.getId();
            String uid = FirebaseAuth.getInstance().getUid();
            if(uid == null) {
                return false;
            }
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("contactid",String.valueOf(contactId));
            params.put("stringid", uid);

            try {
                JSONObject object = new JSONObject(requestHandler.sendPostRequest(ApiCall.ADD_CONTACT_URL, params));
                if (!object.getBoolean("error")) {
                    // type:
                    // 0 - invitation's been sent
                    // 2 - invitation's been accepted
                    int type = object.getInt("result");
                    if(type==0 && contactsOutgoing!=null)
                        contactsOutgoing.add(contactProfile);
                    else if(type==2 && contacts!=null)
                        contacts.add(contactProfile);
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static class DeleteContactTask extends AsyncTask<Integer,Void,Boolean> {
        @Override
        protected Boolean doInBackground(Integer... integers) {
            Integer contactId = integers[0];
            String uid = FirebaseAuth.getInstance().getUid();
            if(uid == null) {
                return false;
            }
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("contactid",String.valueOf(contactId));
            params.put("stringid", uid);

            try {
                JSONObject object = new JSONObject(requestHandler.sendPostRequest(ApiCall.DELETE_CONTACT_URL, params));
                if (!object.getBoolean("error")) {
                    int index = -1;
                    int i = 0;
                    for(Profile p : contacts) {
                        if(p.getId()==contactId) {
                            index = i;
                            break;
                        }
                        i++;
                    }
                    if(index!=-1)
                        contacts.remove(index);
                    index = -1;
                    i = 0;
                    for(Profile p : contactsIncoming) {
                        if(p.getId()==contactId) {
                            index = i;
                            break;
                        }
                        i++;
                    }
                    if(index!=-1)
                        contactsIncoming.remove(index);
                    index = -1;
                    i = 0;
                    for(Profile p : contactsOutgoing) {
                        if(p.getId()==contactId) {
                            index = i;
                            break;
                        }
                        i++;
                    }
                    if(index!=-1)
                        contactsOutgoing.remove(index);
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static class CreateProfileTask extends AsyncTask<Profile, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Profile... profiles) {
            Profile profile = profiles[0];
            String uid = FirebaseAuth.getInstance().getUid();
            if(uid == null) {
                return false;
            }
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("name",profile.getName());
            params.put("surname",profile.getSurname());
            params.put("genderid",String.valueOf(1)); //TODO
            params.put("stringid", uid);

            try {
                JSONObject object = new JSONObject(requestHandler.sendPostRequest(ApiCall.CREATE_PROFILE_URL, params));
                if (!object.getBoolean("error"))
                    return true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static class UpdateProfileTask extends AsyncTask<Void, Void, Boolean> {
        private Profile profile;
        private Drawable picture;
        private String uid;

        public UpdateProfileTask(Profile profile, Drawable picture) {
            this.profile = profile;
            sProfile = profile;
            this.picture = picture;
            uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            if(uid == null)
                return;
            profile.setImagePath("profile_images/" + uid + ".jpg");
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            if(uid == null) {
                return false;
            }
            // TODO uploading picture in different thread
            if(picture!=null) {
                StorageReference reference = FirebaseStorage.getInstance().getReference("profile_images").child(uid + ".jpg");
                Bitmap bitmap = ((BitmapDrawable) picture).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                reference.putBytes(data);

                images.put(profile.getImagePath(), bitmap);
            }
            try {
                String response = sendUpdateProfileRequest(profile);
                JSONObject object = new JSONObject(response);
                if (!object.getBoolean("error"))
                    return true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        private String sendUpdateProfileRequest(Profile profile) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("name",profile.getName());
            params.put("surname",profile.getSurname());
            params.put("genderid",String.valueOf(1)); //TODO
            params.put("stringid",uid);
            params.put("desc",profile.getDescription());
            params.put("imgpath",profile.getImagePath());
            params.put("latitude",String.valueOf(profile.getLatitude()));
            params.put("longitude",String.valueOf(profile.getLongitude()));
            URL url;
            //StringBuilder object to store the message retrieved from the server
            StringBuilder sb = new StringBuilder();
            try {
                //Initializing Url
                url = new URL(ApiCall.UPDATE_PROFILE_URL);

                //Creating an httmlurl connection
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                //Configuring connection properties
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                //Creating an output stream
                OutputStream os = conn.getOutputStream();

                //Writing parameters to the request
                //We are using a method getPostDataString which is defined below
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));

                StringBuilder result = new StringBuilder();
                boolean first = true;
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    if (first)
                        first = false;
                    else
                        result.append("&");

                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                }
                ArrayList<Integer> skills = profile.getMySkills();
                if(skills!=null && !skills.isEmpty()) {
                    for(Integer skillid : skills) {
                        result.append("&");
                        result.append(URLEncoder.encode("cs[]", "UTF-8"));
                        result.append("=");
                        result.append(URLEncoder.encode(String.valueOf(skillid), "UTF-8"));
                    }
                }
                skills = profile.getDesiredSkills();
                if(skills!=null && !skills.isEmpty()) {
                    for(Integer skillid : skills) {
                        result.append("&");
                        result.append(URLEncoder.encode("ds[]", "UTF-8"));
                        result.append("=");
                        result.append(URLEncoder.encode(String.valueOf(skillid), "UTF-8"));
                    }
                }
                writer.write(result.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    sb = new StringBuilder();
                    String response;
                    //Reading server response
                    while ((response = br.readLine()) != null) {
                        sb.append(response);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return sb.toString();
        }
    }

    private static class GetSkillsTask extends AsyncTask<Void, Void, String> {

        Consumer<ArrayList<String>> function;
        ArrayList<Integer> ids;

        GetSkillsTask(Consumer<ArrayList<String>> function, ArrayList<Integer> ids) {
            this.function = function;
            this.ids = ids;
        }
        @Override
        protected String doInBackground(Void... voids) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            return requestHandler.sendGetRequest(ApiCall.GET_SKILLS_URL,null);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    JSONArray jsonArray = object.getJSONArray("skills");
                    HashMap<Integer,String> skillsMap = new HashMap<>();
                    skillNames = new ArrayList<>();
                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        String name = obj.getString("Name");
                        skillsMap.put(obj.getInt("Id"), name);
                        skillNames.add(name);
                    }
                    skills = skillsMap;
                    if(function != null) {
                        if(ids==null) {
                            function.accept(skillNames);
                        }
                        else {
                            ArrayList<String> chosenSkills = new ArrayList<>();
                            for(Integer id : ids) {
                                chosenSkills.add(skills.get(id));
                            }
                            function.accept(chosenSkills);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static class GetProfileTask extends AsyncTask<String,Void,String> {

        Consumer<Profile> function;

        public GetProfileTask(Consumer<Profile> function) {
            this.function = function;
        }

        @Override
        protected String doInBackground(String... strings) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("uid",strings[0]);
            return requestHandler.sendGetRequest(ApiCall.GET_PROFILE_URL,params);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    sProfile = JSONtoProfile(object.getJSONObject("profile"));
                    if(function != null) {
                        function.accept(sProfile);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static class GetNewProfilesTask extends AsyncTask<Void, Void, String> {

        Consumer<ArrayList<Profile>> function;
        int startIndex;
        int count;
        int myId;

        GetNewProfilesTask(Consumer<ArrayList<Profile>> function, int myId, int startIndex, int count) {
            this.function = function;
            this.startIndex = startIndex;
            this.count = count;
            this.myId = myId;
        }
        @Override
        protected String doInBackground(Void... voids) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("start",String.valueOf(startIndex));
            params.put("count",String.valueOf(count));
            return requestHandler.sendGetRequest(ApiCall.GET_NEW_PROFILES_URL, params);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    JSONArray jsonArray = object.getJSONArray("profiles");
                    ArrayList<Profile> profiles = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        profiles.add(JSONtoProfile(obj));
                    }
                    if(newProfiles == null) {
                        newProfiles = profiles;
                        int myIndex = -1;
                        int i = 0;
                        for(Profile p : profiles) {
                            if(p.getId()==myId) {
                                myIndex = i;
                                break;
                            }
                            i++;
                        }
                        if(myIndex!=-1) {
                            newProfiles.remove(myIndex);
                        }
                    }
                    else {
                        newProfiles.addAll(profiles);
                    }
                    if(function!=null)
                        function.accept(newProfiles);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static class GetClosestProfilesTask extends AsyncTask<Void, Void, String> {

        Consumer<ArrayList<Profile>> function;
        double longitude;
        double latitude;
        int startIndex;
        int count;
        int myId;

        GetClosestProfilesTask(Consumer<ArrayList<Profile>> function, int myId, double longitude, double latitude, int startIndex, int count) {
            this.function = function;
            this.longitude = longitude;
            this.latitude = latitude;
            this.startIndex = startIndex;
            this.count = count;
            this.myId = myId;
        }
        @Override
        protected String doInBackground(Void... voids) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("longitude",String.valueOf(longitude));
            params.put("latitude",String.valueOf(latitude));
            params.put("start",String.valueOf(startIndex));
            params.put("count",String.valueOf(count));
            String s = requestHandler.sendGetRequest(ApiCall.GET_CLOSEST_PROFILES_URL, params);

            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    JSONArray jsonArray = object.getJSONArray("profiles");
                    ArrayList<Profile> profiles = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        profiles.add(JSONtoProfile(obj));
                    }
                    if(closestProfiles==null)
                        closestProfiles = new ArrayList<>();
                    for (Profile p: profiles)
                        if(p.getId()!=myId)
                            closestProfiles.add(p);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(function!=null)
                function.accept(closestProfiles);
        }
    }

    private static class GetForYouProfilesTask extends AsyncTask<Void, Void, String> {

        Consumer<ArrayList<Profile>> function;
        int profileId;
        int startIndex;
        int count;
        double longitude;
        double latitude;

        GetForYouProfilesTask(Consumer<ArrayList<Profile>> function, int profileId,  double longitude, double latitude, int startIndex, int count) {
            this.function = function;
            this.profileId = profileId;
            this.startIndex = startIndex;
            this.count = count;
            this.longitude = longitude;
            this.latitude = latitude;
        }
        @Override
        protected String doInBackground(Void... voids) {
            DatabaseRequestHandler requestHandler = new DatabaseRequestHandler();
            HashMap<String,String> params = new HashMap<>();
            params.put("id",String.valueOf(profileId));
            params.put("longitude",String.valueOf(longitude));
            params.put("latitude",String.valueOf(latitude));
            params.put("kmDistance",String.valueOf(searchRadius));
            params.put("start",String.valueOf(startIndex));
            params.put("count",String.valueOf(count));
            return requestHandler.sendGetRequest(ApiCall.GET_FOR_YOU_PROFILES_URL, params);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    JSONArray jsonArray = object.getJSONArray("profiles");
                    ArrayList<Profile> profiles = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        profiles.add(JSONtoProfile(obj));
                    }
                    forYouProfiles = profiles;
                    if(function!=null)
                        function.accept(forYouProfiles);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static Profile JSONtoProfile(JSONObject obj) throws JSONException {
        Profile profile = new Profile();
        profile.setId(obj.getInt("Id"));
        profile.setName(obj.getString("Name"));
        profile.setSurname(obj.getString("Surname"));
        if(profile.getName().endsWith("a"))
            profile.setGender(Gender.woman);
        else
            profile.setGender(Gender.man); // TODO
        String desc = obj.getString("Description");
        if(!desc.equals("null")) {
            profile.setDescription(desc);
        }
        String imgPath = obj.getString("ImagePath");
        if(!imgPath.equals("null")) {
            profile.setImagePath(imgPath);
        }
        try {
            profile.setLongitude(obj.getDouble("Longitude"));
            profile.setLatitude(obj.getDouble("Latitude"));
        }
        catch(JSONException e) {
            profile.setLongitude(0);
            profile.setLatitude(0);
        }
        ArrayList<Integer> skills = new ArrayList<>();
        JSONArray arr = obj.getJSONArray("Current");
        for(int i =0;i<arr.length();i++) {
            skills.add(arr.getInt(i));
        }
        profile.setMySkills(skills);
        skills = new ArrayList<>();
        arr = obj.getJSONArray("Desired");
        for(int i =0;i<arr.length();i++) {
            skills.add(arr.getInt(i));
        }
        profile.setDesiredSkills(skills);
        return profile;
    }


}
