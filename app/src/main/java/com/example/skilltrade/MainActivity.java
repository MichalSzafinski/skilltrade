package com.example.skilltrade;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Consumer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewStub;
import android.widget.TextView;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends NavDrawerActivity{

    public static final int PROFILES_LOAD_COUNT = 15;

    private ScreenSlidePageFragment[] tabFragments;
    private SharedPreferences.OnSharedPreferenceChangeListener mPreferenceChangeListener;
    //The pager widget, which handles animation and allows swiping horizontally to access previous
    //and next wizard steps.
    private ViewPager mPager;
    //The pager adapter, which provides the pages to the view pager widget.
    private PagerAdapter pagerAdapter;

    FirebaseAuth firebaseAuth;

    private FusedLocationProviderClient fusedLocationClient;
    private String UserId;

    private static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 12345;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //If creating another activity, copy from here
        ViewStub viewStub = findViewById(R.id.layout);
        viewStub.setLayoutResource(R.layout.content_main);
        viewStub.inflate();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //to here
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String distStr = sp.getString("distance","100 km");
        int dist = Integer.parseInt(distStr.substring(0,distStr.length() - 3));

        mPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                String distStr = sharedPreferences.getString("distance","100 km");
                int dist = Integer.parseInt(distStr.substring(0,distStr.length() - 3));
                DatabaseHelper.setSearchRadius(dist);
                final Location l = DatabaseHelper.getUserLocation();
                DatabaseHelper.getProfile(new Consumer<Profile>() {
                    @Override
                    public void accept(Profile profile) {
                        DatabaseHelper.getForYouProfiles(new Consumer<ArrayList<Profile>>() {
                            @Override
                            public void accept(ArrayList<Profile> profiles) {
                                tabFragments[0].UpdateData(profiles.toArray(new Profile[profiles.size()]));
                            }
                        },profile.getId(), l.getLongitude(), l.getLatitude(), PROFILES_LOAD_COUNT, true);
                    }
                });
            }
        };
        sp.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);

        InitiateTabFragments();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean firstLaunch = prefs.getBoolean(getString(R.string.pref_first_launch), true);
        if(firstLaunch) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(getString(R.string.pref_first_launch), false);
            editor.commit();
            startActivity(new Intent(getApplicationContext(), OnBoardingActivity.class));
        }

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(mPager);

        firebaseAuth = FirebaseAuth.getInstance();


        //Set database
        mDatabase = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        // Here, thisActivity is the current activity
        while (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        final Location l = location;
                        DatabaseHelper.setUserLocation(location);
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            DatabaseHelper.getProfile(new Consumer<Profile>() {
                                @Override
                                public void accept(Profile profile) {
                                    DatabaseHelper.getClosestProfiles(new Consumer<ArrayList<Profile>>() {
                                        @Override
                                        public void accept(ArrayList<Profile> profiles) {
                                            tabFragments[1].UpdateData(profiles.toArray(new Profile[profiles.size()]));
                                        }
                                    }, profile.getId(), l.getLongitude(), l.getLatitude(), PROFILES_LOAD_COUNT);
                                    profile.setLatitude(l.getLatitude());
                                    profile.setLongitude(l.getLongitude());
                                    new DatabaseHelper.UpdateProfileTask(profile, null).execute();
                                    DatabaseHelper.getForYouProfiles(new Consumer<ArrayList<Profile>>() {
                                        @Override
                                        public void accept(ArrayList<Profile> profiles) {
                                            tabFragments[0].UpdateData(profiles.toArray(new Profile[profiles.size()]));
                                        }
                                    },profile.getId(), l.getLongitude(), l.getLatitude(), PROFILES_LOAD_COUNT);
                                }
                            });
                        }
                    }
                });
        DatabaseHelper.getProfile(new Consumer<Profile>() {
            @Override
            public void accept(Profile profile) {
                DatabaseHelper.getNewProfiles(new Consumer<ArrayList<Profile>>() {
                    @Override
                    public void accept(ArrayList<Profile> profiles) {
                        tabFragments[2].UpdateData(profiles.toArray(new Profile[profiles.size()]));
                    }
                },profile.getId(),PROFILES_LOAD_COUNT);
            }
        });
    }

    private void InitiateTabFragments()
    {
        tabFragments = new ScreenSlidePageFragment[3];

        ScreenSlidePageFragment closestTabFragment = new ScreenSlidePageFragment();
        closestTabFragment.setFragmentType(ScreenSlidePageFragment.FragmentType.Closest);
        tabFragments[1] = closestTabFragment;

        ScreenSlidePageFragment newTabFragment = new ScreenSlidePageFragment();
        newTabFragment.setFragmentType(ScreenSlidePageFragment.FragmentType.New);
        tabFragments[2] = newTabFragment;

        ScreenSlidePageFragment forYouTabFragment = new ScreenSlidePageFragment();
        forYouTabFragment.setFragmentType(ScreenSlidePageFragment.FragmentType.ForYou);
        tabFragments[0] = forYouTabFragment;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            if (mPager.getCurrentItem() == 0) {
                // If the user is currently looking at the first step, allow the system to handle the
                // Back button. This calls finish() on this activity and pops the back stack.
                super.onBackPressed();
            } else {
                // Otherwise, select the previous step.
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.nav_log_out:
                DatabaseHelper.ClearData();
                FirebaseAuth.getInstance().signOut();
                break;
            case R.id.nav_my_profile:
                Intent intent = new Intent(this, MyProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_contacts:
                startActivity(new Intent(this, ContactsActivity.class));
                break;
            case R.id.nav_settings:
                super.StartSettingsActivity();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // here we different fragments for each sliding tab can be added
            return tabFragments[position];
        }

        @Override
        public int getCount() {
            return 3;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position)
            {
                case 1:
                    return getResources().getString(R.string.sliding_tab_closest);
                case 2:
                    return getResources().getString(R.string.sliding_tab_new);
                case 0:
                    return getResources().getString(R.string.sliding_tab_for_you);
            }
            return "Item " + position;
        }
    }
}
