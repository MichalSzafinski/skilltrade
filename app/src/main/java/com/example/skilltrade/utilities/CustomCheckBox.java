package com.example.skilltrade.utilities;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.skilltrade.R;

public class CustomCheckBox extends RelativeLayout {
    private TextView mTextView;
    private ImageView mImageView;
    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        if(isChecked)
            mImageView.setVisibility(VISIBLE);
        else
            mImageView.setVisibility(INVISIBLE);
    }

    public void setText(String text) {
        mTextView.setText(text);
    }

    public String getText() {
        return mTextView.getText().toString();
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CustomCheckBox, 0, 0);
        String text = a.getString(R.styleable.CustomCheckBox_text);
        int textColor = a.getColor(R.styleable.CustomCheckBox_textColor, Color.BLACK);
        float textSize = a.getDimensionPixelSize(R.styleable.CustomCheckBox_textSize,42);
        a.recycle();

        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_check_box, this, true);

        mTextView = (TextView) getChildAt(0);
        mImageView = (ImageView) getChildAt(1);

        mTextView.setText(text);
        mTextView.setTextColor(textColor);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

        super.setOnClickListener(defaultOnClickListener);
    }

    public CustomCheckBox(Context context) {
        this(context, null);
    }

    @Override
    public void setOnClickListener(View.OnClickListener l) {
        super.setOnClickListener(defaultOnClickListener);
        otherListener = l;
    }
    private OnClickListener otherListener;
    private OnClickListener defaultOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            setChecked(!isChecked);
            if(otherListener!=null)
                otherListener.onClick(v);
        }
    };
}