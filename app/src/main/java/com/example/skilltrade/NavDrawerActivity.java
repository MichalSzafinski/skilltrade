package com.example.skilltrade;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.util.Consumer;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NavDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    static protected FirebaseUser firebaseUser;
    static protected DatabaseReference mDatabase;
    static protected StorageReference storageRef;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener  authStateListener;
    static private Profile myProfile = null;

    public ImageView navDrawerImage;
    public TextView navName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user == null){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        };
        try
        {
            firebaseUser = firebaseAuth.getCurrentUser();
        }
        catch (Exception e)
        {
            Log.e("NavDrawerActivity","getting firebase user error" + e.toString());
        }
//        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        if(user==null) {
//            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//            finish();
//        }

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        final TextView textViewLogged = hView.findViewById(R.id.textViewLogged);
        final ImageView imageViewHeader = hView.findViewById(R.id.imageView);
        navDrawerImage = imageViewHeader;
        navName = textViewLogged;

        if(textViewLogged!=null && imageViewHeader!=null)
        {
            if(myProfile==null)
            {
                DatabaseHelper.getProfile(new Consumer<Profile>() {
                    @Override
                    public void accept(Profile profile) {
                        myProfile = profile;
                        textViewLogged.setText(myProfile.getName() + " " + myProfile.getSurname());

                        DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                            @Override
                            public void accept(List<Bitmap> bitmaps) {
                                navDrawerImage.setImageDrawable(new BitmapDrawable(getResources(), bitmaps.get(0)));
                            }
                        }, new ArrayList<>(Arrays.asList(myProfile)));

                    }
                });
            }
            else
            {
                textViewLogged.setText(myProfile.getName() + " " + myProfile.getSurname());
                DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                    @Override
                    public void accept(List<Bitmap> bitmaps) {
                        navDrawerImage.setImageDrawable(new BitmapDrawable(getResources(), bitmaps.get(0)));
                    }
                }, new ArrayList<>(Arrays.asList(myProfile)));
            }
        }
        else
            Log.e("NavDrawerActivity","textViewLogged null!!!!!!!");


    }
    @Override
    protected void onResume() {
        super.onResume();
        DatabaseHelper.cleared = false;

        DatabaseHelper.getProfile(new Consumer<Profile>() {
            @Override
            public void accept(Profile profile) {
                if(profile!=null)
                {
                    myProfile = profile;
                    navName.setText(myProfile.getName() + " " + myProfile.getSurname());
                }

                DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                    @Override
                    public void accept(List<Bitmap> bitmaps) {
                        navDrawerImage.setImageDrawable(new BitmapDrawable(getResources(), bitmaps.get(0)));
                    }
                }, new ArrayList<>(Arrays.asList(profile)));
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(authStateListener!=null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            StartSettingsActivity();
        }

        return super.onOptionsItemSelected(item);
    }
    protected void StartSettingsActivity()
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        switch(id) {
            case R.id.nav_main_page:
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.nav_log_out:
                DatabaseHelper.ClearData();
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.nav_my_profile:
                intent = new Intent(this, MyProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.nav_contacts:
                intent = new Intent(this, ContactsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.nav_settings:
                StartSettingsActivity();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
