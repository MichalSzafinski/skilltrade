package com.example.skilltrade;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.Consumer;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

public class ScreenSlidePageFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;
    private LinearLayoutManager layoutManager;
    private View myView;
    public Profile[] dataset = new Profile[]{};
    public Bitmap[] imageset;
    public ProgressBar mProgressBar;
    public FragmentType type;
    public enum FragmentType
    {
        ForYou, Closest, New,
    }

    ReentrantLock lock = new ReentrantLock();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        mProgressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        mAdapter.recyclerView = recyclerView;
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean limitScrollReached = false;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if(limitScrollReached==false && dataset.length - 1 == layoutManager.findLastCompletelyVisibleItemPosition())
                {

                    switch(type)
                    {
                        case Closest:
                            final Location location = DatabaseHelper.getUserLocation();
                            DatabaseHelper.getProfile(new Consumer<Profile>() {
                                @Override
                                public void accept(Profile profile) {
                                    double longitude = 0, latitude = 0;
                                    if(location!=null) {
                                        longitude = location.getLongitude();
                                        latitude = location.getLatitude();
                                    }
                                    DatabaseHelper.getClosestProfiles(new Consumer<ArrayList<Profile>>() {
                                        @Override
                                        public void accept(ArrayList<Profile> profiles) {
                                            if(profiles.size()>dataset.length)
                                                UpdateData(profiles.toArray(new Profile[profiles.size()]));
                                            else
                                                limitScrollReached = true;
                                        }
                                    },profile.getId(),longitude, latitude, dataset.length + MainActivity.PROFILES_LOAD_COUNT);
                                }
                            });
                            mProgressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            break;
                        case New:
                            DatabaseHelper.getProfile(new Consumer<Profile>() {
                                @Override
                                public void accept(Profile profile) {
                                    DatabaseHelper.getNewProfiles(new Consumer<ArrayList<Profile>>() {
                                        @Override
                                        public void accept(ArrayList<Profile> profiles) {
                                            if(profiles.size()>dataset.length)
                                                UpdateData(profiles.toArray(new Profile[profiles.size()]));
                                            else
                                                limitScrollReached = true;
                                        }
                                    }, profile.getId(),dataset.length + MainActivity.PROFILES_LOAD_COUNT);
                                }
                            });
                            mProgressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            break;
                        case ForYou:
                            final Location location2 = DatabaseHelper.getUserLocation();
                            DatabaseHelper.getProfile(new Consumer<Profile>() {
                                @Override
                                public void accept(Profile profile) {
                                    double longitude = 0, latitude = 0;
                                    if(location2!=null) {
                                        longitude = location2.getLongitude();
                                        latitude = location2.getLatitude();
                                    }
                                    DatabaseHelper.getForYouProfiles(new Consumer<ArrayList<Profile>>() {
                                        @Override
                                        public void accept(ArrayList<Profile> profiles) {
                                            if(profiles.size()>dataset.length)
                                                UpdateData(profiles.toArray(new Profile[profiles.size()]));
                                            else
                                                limitScrollReached = true;
                                        }
                                    }, profile.getId(),longitude, latitude, dataset.length + MainActivity.PROFILES_LOAD_COUNT);
                                }
                            });
                            mProgressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            break;
                    }

                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        if(dataset!=null) {
            mProgressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        myView = rootView;
        return rootView;
    }
    public void setFragmentType(FragmentType t)
    {
        type = t;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if(mAdapter == null)
        {
            mAdapter = new RecyclerAdapter(dataset, getActivity());
        }
        if(imageset == null)
        {
            UpdateData(dataset);
        }
        super.onCreate(savedInstanceState);
    }

    public Boolean UpdateData(Profile[] profiles)
    {
        dataset = profiles;
        if(mAdapter != null) {
            mAdapter.setDataset(profiles);

            mAdapter.notifyDataSetChanged();
            if(myView != null)
            {
                mProgressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            //new DownloadImages().execute();
            DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
                @Override
                public void accept(List<Bitmap> bitmaps) {
                    imageset = new Bitmap[bitmaps.size()];
                    for (int i=0;i<bitmaps.size();i++)
                    {
                        imageset[i] = bitmaps.get(i);
                    }
                    mAdapter.setImageSet(imageset);
                    mAdapter.notifyDataSetChanged();
                }
            }, Arrays.asList(profiles));



            return true;
        }
        return false;
    }

}
