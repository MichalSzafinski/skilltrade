package com.example.skilltrade;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.InputStream;
import java.util.ArrayList;

public class AccountSetupActivity extends AppCompatActivity {

    public static final int PICK_IMAGE = 1;
    public static final int CHOOSE_CURRENT_SKILLS = 2;
    public static final int CHOOSE_DESIRED_SKILLS = 3;

    private ImageView mImageView;
    private ImageView[] stepIndicators;
    private LinearLayout layoutStepIndicators;
    private ViewPager mViewPager;
    private AccountSetupAdapter mAccountSetupAdapter;
    private Button btnBack;
    private Button btnNext;
    private FirebaseUser user;
    private Profile userProfile;
    private Uri profileImageUri;
    private Drawable profilePicture;
    private String description = "";
    private ArrayList<Integer> currentSkills;
    private ArrayList<Integer> desiredSkills;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setup);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
        userProfile = new Profile();
        if (getIntent().getExtras().containsKey("userName"))
            userProfile.setName(getIntent().getExtras().getString("userName"));

        if (getIntent().getExtras().containsKey("userSurname"))
            userProfile.setSurname(getIntent().getExtras().getString("userSurname"));

        if (getIntent().getExtras().containsKey("userGender"))
            userProfile.setGender((Gender) getIntent().getExtras().getSerializable("userGender"));

        layoutStepIndicators = findViewById(R.id.view_step_indicators);
        mViewPager = findViewById(R.id.pager_account_setup);
        btnBack = findViewById(R.id.btn_back);
        btnNext = findViewById(R.id.btn_next);

        mAccountSetupAdapter = new AccountSetupAdapter(this);
        mViewPager.setAdapter(mAccountSetupAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < stepIndicators.length; i++) {
                    if (i <= position)
                        stepIndicators[i].setImageDrawable(ContextCompat.getDrawable(AccountSetupActivity.this, R.drawable.step_done_indicator));
                    else
                        stepIndicators[i].setImageDrawable(ContextCompat.getDrawable(AccountSetupActivity.this, R.drawable.step_indicator));
                }
                if (position == 0) {
                    btnBack.setVisibility(View.INVISIBLE);
                } else {
                    btnBack.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mViewPager.getCurrentItem() - 1;
                if (position >= 0) {
                    mViewPager.setCurrentItem(position);
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() == 1) { // Description page
                    description = mAccountSetupAdapter.getDescription();
                }
                int position = mViewPager.getCurrentItem() + 1;
                if (position < mAccountSetupAdapter.getCount()) {
                    mViewPager.setCurrentItem(position);
                } else {
                    saveUserDataToDatabase();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        createStepIndicators();
    }

    private void saveUserDataToDatabase() {
        userProfile.setDescription(description);
        userProfile.setLatitude(0); // TODO
        userProfile.setLongitude(0);
        userProfile.setMySkills(currentSkills);
        userProfile.setDesiredSkills(desiredSkills);
        new DatabaseHelper.UpdateProfileTask(userProfile,profilePicture).execute();
    }

    private void createStepIndicators() {
        stepIndicators = new ImageView[mAccountSetupAdapter.getCount()];
        layoutStepIndicators.setWeightSum(stepIndicators.length);
        for (int i = 0; i < stepIndicators.length; i++) {
            stepIndicators[i] = new ImageView(this);
            stepIndicators[i].setImageDrawable(ContextCompat.getDrawable(AccountSetupActivity.this, R.drawable.step_indicator));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    getResources().getDimensionPixelSize(R.dimen.step_indicator_width),
                    LinearLayout.LayoutParams.MATCH_PARENT, 1
            );
            params.setMargins(3, 0, 3, 0);
            layoutStepIndicators.addView(stepIndicators[i], params);
        }
        stepIndicators[0].setImageDrawable(ContextCompat.getDrawable(AccountSetupActivity.this, R.drawable.step_done_indicator));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    profileImageUri = data.getData();
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(profileImageUri);
                        profilePicture = Drawable.createFromStream(inputStream, profileImageUri.toString());
                        mAccountSetupAdapter.setProfilePicture(profilePicture);
                    } catch (Exception e) {
                        Log.i("AccountSetupActivity", "Error while loading image");
                    }
                }
                break;
            case CHOOSE_CURRENT_SKILLS:
                ArrayList<Integer> currentSkillsList = (ArrayList<Integer>) data.getSerializableExtra("skillsList");
                currentSkills = currentSkillsList;
                mAccountSetupAdapter.setCurrentSkills(currentSkillsList);
                break;
            case CHOOSE_DESIRED_SKILLS:
                ArrayList<Integer> desiredSkillsList = (ArrayList<Integer>) data.getSerializableExtra("skillsList");
                desiredSkills = desiredSkillsList;
                mAccountSetupAdapter.setDesiredSkills(desiredSkillsList);
                break;
        }
    }

    public Drawable getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Drawable profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Integer> getCurrentSkills() {
        return currentSkills;
    }

    public void setCurrentSkills(ArrayList<Integer> currentSkills) {
        this.currentSkills = currentSkills;
    }

    public ArrayList<Integer> getDesiredSkills() {
        return desiredSkills;
    }

    public void setDesiredSkills(ArrayList<Integer> desiredSkills) {
        this.desiredSkills = desiredSkills;
    }

    public Profile getUserProfile() {
        return userProfile;
    }
}

