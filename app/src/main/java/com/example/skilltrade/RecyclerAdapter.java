package com.example.skilltrade;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Consumer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private Profile[] mDataset;
    private Bitmap[] imageSet;
    public RecyclerView recyclerView;
    private Activity activity;
    private final View.OnClickListener profileRecyclerOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int item_position = recyclerView.getChildAdapterPosition(v);
            Intent intent = new Intent(activity, ProfileActivity.class);
            intent.putExtra("profile", mDataset[item_position]);
            if(imageSet!=null)
            {
//                if(imageSet[item_position]!=null) {
////                    String path = saveToInternalStorage(imageSet[item_position]);
////                    intent.putExtra("imagePath",path);
//                }
                for (int i=0; i<imageSet.length; i++)
                {
                    Log.i("IMAGE" + i, imageSet[i]==null?"null":imageSet[i].toString());
                }
            }
            for (int i=0; i<mDataset.length; i++)
            {
                Log.i("profile " + i, mDataset[i]==null?"null":mDataset[i].toString());
            }
            activity.startActivity(intent);
        }
    };
//    private String saveToInternalStorage(Bitmap bitmapImage){
//        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//        // path to /data/data/yourapp/app_data/imageDir
//        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//        // Create imageDir
//        File mypath=new File(directory,"profile.jpg");
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(mypath);
//            // Use the compress method on the BitMap object to write image to the OutputStream
//            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                fos.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return directory.getAbsolutePath();
//    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewName;
        public TextView textViewSurname;
        public ImageView imageView;
        public TextView textViewCurrentSkills;
        public TextView textViewDesiredSkills;

        public MyViewHolder(View v) {
            super(v);

            textViewName = v.findViewById(R.id.textViewName);
            textViewSurname = v.findViewById(R.id.textViewSurname);
            imageView = v.findViewById(R.id.imageView);
            textViewCurrentSkills = v.findViewById(R.id.currentSkillsView);
            textViewDesiredSkills = v.findViewById(R.id.desiredSkillsView);
        }
    }

    public RecyclerAdapter(Profile[] myDataset, Activity c)
    {
        activity = c;
        mDataset = myDataset;
    }

    public void setDataset(Profile[] mDataset) {
        this.mDataset = mDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        v.setOnClickListener(profileRecyclerOnClickListener);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Profile profile = mDataset[position];
        holder.textViewName.setText(profile.getName());
        holder.textViewSurname.setText(profile.getSurname());

        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                if(strings.isEmpty()) {
                    holder.textViewCurrentSkills.setText(R.string.no_skills);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for(String skill : strings) {
                    sb.append(skill + "\n");
                }
                holder.textViewCurrentSkills.setText(sb.toString());
            }
        },profile.getMySkills());

        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                if(strings.isEmpty()) {
                    holder.textViewDesiredSkills.setText(R.string.no_skills);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for(String skill : strings) {
                    sb.append(skill + "\n");
                }
                holder.textViewDesiredSkills.setText(sb.toString());
            }
        },profile.getDesiredSkills());

        if(imageSet!=null && position<imageSet.length && imageSet[position] != null)
        {
            holder.imageView.setImageBitmap(imageSet[position]);
        }
        else if(profile.getGender() == Gender.man)
            holder.imageView.setImageResource(R.drawable.default_user_male);
        else
            holder.imageView.setImageResource(R.drawable.default_user_female);
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
    public void setImageSet(Bitmap[] imageSet) {
        this.imageSet = imageSet;
    }

}
