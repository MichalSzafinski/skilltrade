package com.example.skilltrade;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {
    private EditText etName;
    private EditText etSurname;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnSignUp;
    private RadioGroup rgGender;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etName = findViewById(R.id.input_name);
        etSurname = findViewById(R.id.input_surname);
        etEmail = findViewById(R.id.input_email);
        etPassword = findViewById(R.id.input_password);
        btnSignUp = findViewById(R.id.btn_signup);
        rgGender = findViewById(R.id.radio_group_gender);

        firebaseAuth = FirebaseAuth.getInstance();

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        if(firebaseAuth.getCurrentUser()!=null){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void signUp() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_fill_required_fields), Toast.LENGTH_SHORT).show();
            return;
        }
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String name = etName.getText().toString();
                    String surname = etSurname.getText().toString();
                    Gender gender = Gender.man;
                    switch (rgGender.getCheckedRadioButtonId()) {
                        case R.id.radio_male:
                            gender = Gender.man;
                            break;
                        case R.id.radio_female:
                            gender = Gender.woman;
                            break;
                    }

                    Profile profile = new Profile(name,surname);
                    profile.setGender(gender); // TODO

                    new DatabaseHelper.CreateProfileTask().execute(profile);

                    Intent intent = new Intent(getApplicationContext(),AccountSetupActivity.class);
                    intent.putExtra("userName",name);
                    intent.putExtra("userSurname",surname);

                    intent.putExtra("userGender", gender);
                    startActivity(intent);
                    finish();
                } else {
                    Exception ex = task.getException();
                    Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show(); //TODO
                }
            }
        });
    }
}
