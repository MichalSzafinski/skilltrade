package com.example.skilltrade;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.Consumer;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class InvitationsAdapter extends RecyclerView.Adapter<InvitationsAdapter.MyViewHolder> {
    private Profile[] mDataset;
    private Bitmap[] imageSet;
    private Activity activity;
    private RecyclerView recyclerView;

    public Type type;
    public enum Type
    {
        Incoming, Outgoing
    }

    public InvitationsAdapter(Profile[] myDataset, Activity c, RecyclerView rv, Type t)
    {
        activity = c;
        mDataset = myDataset;
        recyclerView = rv;
        type = t;
    }

    @Override
    public InvitationsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacts_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item_position = recyclerView.getChildAdapterPosition(v);
                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("profile", mDataset[item_position]);
                if(imageSet[item_position]!=null)
                {
                    String path = saveToInternalStorage(imageSet[item_position]);
                    intent.putExtra("imagePath",path);
                }
                for (int i=0; i<imageSet.length; i++)
                {
                    Log.i("IMAGE" + i, imageSet[i]==null?"null":imageSet[i].toString());
                }
                for (int i=0; i<mDataset.length; i++)
                {
                    Log.i("profile " + i, mDataset[i]==null?"null":mDataset[i].toString());
                }
                activity.startActivity(intent);
            }
        });
        return new InvitationsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final InvitationsAdapter.MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Profile profile = mDataset[position];
        holder.textViewName.setText(profile.getName());
        holder.textViewSurname.setText(profile.getSurname());
        if(imageSet!=null && position<imageSet.length && imageSet[position] != null)
        {
            holder.imageView.setImageBitmap(imageSet[position]);
        }
        else if(profile.getGender() == Gender.man)
            holder.imageView.setImageResource(R.drawable.default_user_male);
        else
            holder.imageView.setImageResource(R.drawable.default_user_female);
        switch(type) {
            case Incoming:
                holder.imageDelete.setVisibility(View.VISIBLE);
                holder.imageDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int index = recyclerView.getChildAdapterPosition(holder.rootView);
                        final Profile p = mDataset[index];
                        final int contactId = p.getId();
                        new AlertDialog.Builder(activity)
                                .setTitle("Reject")
                                .setMessage("Are you sure you want to reject this invitation?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        new DatabaseHelper.DeleteContactTask().execute(contactId);
                                        Profile[] newDataset = new Profile[mDataset.length-1];
                                        int i = 0;
                                        for(Profile sp : mDataset) {
                                            if(sp.getId()!=p.getId()) {
                                                newDataset[i++] = sp;
                                            }
                                        }
                                        mDataset = newDataset;
                                        InvitationsAdapter.this.notifyDataSetChanged();
                                    }
                                })
                                .setNegativeButton(android.R.string.no,null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
                holder.imageAdd.setVisibility(View.VISIBLE);
                holder.imageAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int index = recyclerView.getChildAdapterPosition(holder.rootView);
                        final Profile p = mDataset[index];
                        new DatabaseHelper.AddContactTask().execute(p);
                        Profile[] newDataset = new Profile[mDataset.length-1];
                        int i = 0;
                        for(Profile sp : mDataset) {
                            if(sp.getId()!=p.getId()) {
                                newDataset[i++] = sp;
                            }
                        }
                        mDataset = newDataset;
                        InvitationsAdapter.this.notifyDataSetChanged();
                    }
                });
                break;
            case Outgoing:
                holder.imageDelete.setVisibility(View.VISIBLE);
                holder.imageDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int index = recyclerView.getChildAdapterPosition(holder.rootView);
                        final Profile p = mDataset[index];
                        final int contactId = p.getId();
                        new DatabaseHelper.DeleteContactTask().execute(contactId);
                        Profile[] newDataset = new Profile[mDataset.length-1];
                        int i = 0;
                        for(Profile sp : mDataset) {
                            if(sp.getId()!=p.getId()) {
                                newDataset[i++] = sp;
                            }
                        }
                        mDataset = newDataset;
                        InvitationsAdapter.this.notifyDataSetChanged();
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public void setDataset(Profile[] mDataset) {
        this.mDataset = mDataset;
    }

    public void setImageSet(Bitmap[] imageSet) {
        this.imageSet = imageSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView textViewName;
        public TextView textViewSurname;
        public ImageView imageView;
        public ImageView imageDelete;
        public ImageView imageAdd;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            textViewName = v.findViewById(R.id.textViewName);
            textViewSurname = v.findViewById(R.id.textViewSurname);
            imageView = v.findViewById(R.id.imageView);
            imageDelete = v.findViewById(R.id.imageDelete);
            imageAdd = v.findViewById(R.id.imageAdd);
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
}
