package com.example.skilltrade.models;

import com.example.skilltrade.enums.Gender;

import java.io.Serializable;
import java.util.ArrayList;


public class Profile implements Serializable {
    private Integer id = 0;
    private String name = "";
    private String surname = "";
    private String description = "";
    private Gender gender;
    private double latitude = 0;
    private double longitude = 0;
    private ArrayList<Integer> mySkills = new ArrayList<Integer>();
    private ArrayList<Integer> desiredSkills = new ArrayList<Integer>();
    private String imagePath = "";

    public Profile(Integer id, String name, String surname, String description, double latitude, double longitude, ArrayList<Integer> mySkills, ArrayList<Integer> desiredSkills, String imagePath) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mySkills = mySkills;
        this.desiredSkills = desiredSkills;
        this.imagePath = imagePath;
    }

    public Profile(String name, String surname, String description) {
        this.name = name;
        this.surname = surname;
        this.description = description;
    }

    public Profile(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Profile() {
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", description='" + description + '\'' +
                ", gender=" + gender +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", mySkills=" + mySkills +
                ", desiredSkills=" + desiredSkills +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDescription() {
        return description;
    }

    public Gender getGender() { return gender; }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public ArrayList<Integer> getMySkills() {
        return mySkills;
    }

    public ArrayList<Integer> getDesiredSkills() {
        return desiredSkills;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGender(Gender gender) { this.gender = gender; }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setMySkills(ArrayList<Integer> mySkills) {
        this.mySkills = mySkills;
    }

    public void setDesiredSkills(ArrayList<Integer> desiredSkills) {
        this.desiredSkills = desiredSkills;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
