<?php 
	class DbConnect{

		private $pdo; 

		function __construct(){}

		function connect(){
			$dsn = getenv('MYSQL_DSN');
			$user = getenv('MYSQL_USER');
			$password = getenv('MYSQL_PASSWORD');
			$this->pdo = new PDO($dsn, $user, $password);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $this->pdo; 
		}
	}
?>