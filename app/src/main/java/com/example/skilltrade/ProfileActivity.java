package com.example.skilltrade;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skilltrade.enums.Gender;
import com.example.skilltrade.models.Profile;
import com.example.skilltrade.utilities.DatabaseHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    private Profile profile;
    private Bitmap profileImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        profileImage=null;
        String imagePath = null;

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                profile= null;
            } else {
                profile= (Profile) extras.getSerializable("profile");
//                imagePath = extras.getString("imagePath");
                //loadImageFromStorage(getApplicationContext().getDir("imageDir", Context.MODE_PRIVATE).getPath());
            }
        } else {
            profile= (Profile) savedInstanceState.getSerializable("profile");
//            imagePath = savedInstanceState.getString("imagePath");
            //loadImageFromStorage(getApplicationContext().getDir("imageDir", Context.MODE_PRIVATE).getPath());
        }
        final ImageView addContactView = findViewById(R.id.add_contact_image);
        addContactView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseHelper.AddContactTask().execute(profile);
                addContactView.setVisibility(View.GONE);
            }
        });

        List<Profile> thisProfile = new ArrayList<>();
        thisProfile.add(profile);
        UpdateImage(); // To set default
        DatabaseHelper.getImages(new Consumer<List<Bitmap>>() {
            @Override
            public void accept(List<Bitmap> bitmaps) {
                profileImage = bitmaps.get(0);
                UpdateImage();
            }
        },thisProfile);
//        loadImageFromStorage(imagePath);

        UpdateText();
    }
//    private void loadImageFromStorage(String path)
//    {
//        if(path == null)
//        {
//            Log.e("loadImageFromStorage","path is null!");
//            profileImage=null;
//            return;
//        }
//        try {
//            File f = new File(path, "profile.jpg");
//            profileImage = BitmapFactory.decodeStream(new FileInputStream(f));
//        }
//        catch (FileNotFoundException e)
//        {
//            profileImage = null;
//            Log.e("loadImageFromStorage",e.getMessage());
//            e.printStackTrace();
//        }
//    }
    public void UpdateImage()
    {
        CircleImageView imageView = findViewById(R.id.profile_image);
        if(profileImage!=null)
        {
            imageView.setImageBitmap(profileImage);
        }
        else if(profile.getGender() == Gender.man)
            imageView.setImageResource(R.drawable.default_user_male);
        else
            imageView.setImageResource(R.drawable.default_user_female);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void UpdateText()
    {
        final Profile result = profile;

        TextView t;
        t = findViewById(R.id.textViewName);
        String name = result.getName() + " " + result.getSurname();
        t.setText(name);

        t = findViewById(R.id.textViewDescription);
        String descr = getResources().getString(R.string.description) + ": " + result.getDescription();
        if(result.getDescription().length() == 0)
            descr += "-";
        t.setText(descr);

        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                TextView t = findViewById(R.id.textViewMySkills);
                String mySkills = getResources().getString(R.string.my_skills) + ": ";
                for (String skill: strings) {
                    mySkills += skill + ", ";
                }
                if(result.getMySkills().size() == 0)
                    mySkills += "-";
                else
                    mySkills = mySkills.substring(0,mySkills.length()-2);
                t.setText(mySkills);
            }
        }, result.getMySkills());

        DatabaseHelper.getSkills(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) {
                TextView t=findViewById(R.id.textViewDesiredSkills);
                String desiredSkills = getResources().getString(R.string.desired_skills) + ": ";
                for (String skill: strings) {
                    desiredSkills += skill + ", ";
                }
                if(result.getDesiredSkills().size() == 0)
                    desiredSkills += "-";
                else
                    desiredSkills = desiredSkills.substring(0,desiredSkills.length()-2);
                t.setText(desiredSkills);
            }
        }, result.getDesiredSkills());
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
